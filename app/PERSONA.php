<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PERSONA extends Model
{
	protected $primaryKey = 'idPersona';
    protected $table = 'PERSONA';
    public $timestamps = false;

    public function usuario(){

    	return $this->belongsTo('App\USUARIO','idUsuario');

    }
}
