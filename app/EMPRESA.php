<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EMPRESA extends Model
{
	protected $primaryKey = 'idempresa';
    protected $table = 'EMPRESA';
    public $timestamps = false;
}
