<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gerentes extends Model
{
	protected $primaryKey = 'idgerente';
    protected $table = 'gerentes';
    public $timestamps = false;
}
