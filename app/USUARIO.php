<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class USUARIO extends Model
{
	protected $primaryKey = 'idUsuario';
    protected $table = 'USUARIO';
    protected $hidden = ['contra'];
    public $timestamps = false;


    public function persona(){

    	return $this->belongsTo('App\PERSONA','idUsuario');

    }


}
