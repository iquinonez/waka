<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class oportunidades extends Model
{
	protected $primaryKey = 'idoportunidad';
    protected $table = 'oportunidades';
    public $timestamps = false;
}
