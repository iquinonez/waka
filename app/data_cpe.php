<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_cpe extends Model
{
	protected $primaryKey = 'iddata';
    protected $table = 'data_cpe';
    public $timestamps = false;
}
