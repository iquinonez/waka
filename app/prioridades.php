<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prioridades extends Model
{
	protected $primaryKey = 'idprioridad';
    protected $table = 'prioridades';
    public $timestamps = false;
}
