<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PERFIL extends Model
{
	protected $primaryKey = 'idPerfil';
    protected $table = 'PERFIL';
    public $timestamps = false;
}
