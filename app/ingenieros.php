<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ingenieros extends Model
{
	protected $primaryKey = 'idingeniero';
    protected $table = 'ingenieros';
    public $timestamps = false;
}
