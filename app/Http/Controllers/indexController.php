<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App\USUARIO;
use App\PERSONA;
use App\EMPRESA;
use App\PERFILXOPCION;
use App\USUARIOXPERFIL;
use Illuminate\Support\Facades\DB;
use App\Classes\dsClass;
use App\Mail\forgot_password;
use Hash;


class indexController extends Controller
{

    public function index()
    {

        //return session('perfil_uactivo');
    	return view::make('index.index');
    }

    public function login(Request $request)
    {
    	$nombreUsuario = $request->input( 'nombreusuario' );
        $contraUsuario = sha1($request->input( 'contrausuario' ));

        $validar = USUARIO::where('nombreUsuario',$nombreUsuario)
        ->where('contra', $contraUsuario)
        ->get();

        if($validar->count() > 0)
        {

            $permisos = USUARIO::leftJoin('USUARIOXPERFIL as up','up.idUsuario','=','USUARIO.idUsuario')
            ->leftJoin('PERFIL as p','p.idPerfil','=','up.idPerfil')
            ->leftJoin('PERSONA as per','per.idUsuario','=','USUARIO.idUsuario')
            ->where('USUARIO.idUsuario',$validar->first()->idUsuario)
            //->where('up.idPerfil',1)
            ->select('USUARIO.idUsuario','USUARIO.nombreUsuario','p.idPerfil','p.nombre as nombrePerfil','per.nombre as nombrePersona')
            ->first();
        }
        else{
            return 'no';
        }

        

    if($permisos != ''){

        if($permisos['idPerfil'] != 0){

            $permisos = USUARIO::leftJoin('USUARIOXPERFIL as up','up.idUsuario','=','USUARIO.idUsuario')
            ->leftJoin('PERFIL as p','p.idPerfil','=','up.idPerfil')
            ->leftJoin('PERSONA as per','per.idUsuario','=','USUARIO.idUsuario')
            ->where('USUARIO.idUsuario',$validar->first()->idUsuario)
            //->where('up.idPerfil',1)
            ->select('USUARIO.idUsuario','USUARIO.nombreUsuario','p.idPerfil','p.nombre as nombrePerfil','per.nombre as nombrePersona')
            ->first();

                session()->put('id_uactivo',$permisos->idUsuario);
                session()->put('nombre_uactivo',$permisos->nombrePersona);
                session()->put('perfil_uactivo',$permisos->nombrePerfil);
                session()->put('correo_uactivo',$permisos->nombreUsuario);
                session()->put('cargo',$permisos->idPerfil);
                $perm2=PERSONA::leftJoin('ingenieros as ing','ing.idpersona','=','PERSONA.idPersona')
                        ->where('PERSONA.idUsuario',$validar->first()->idUsuario)
                        ->where('ing.estado',1)
                        ->get();
                if (count($perm2)>0) {
                    session()->put('id_ing',$perm2->first()->idUsuario);
                }
                else
                {
                    session()->put('id_ing',0);
                }
                
                //session['nanme'] en blade
                //session('name') en controller

             return 'ok';

            
        }
        else{
            return 'no';
        }

    }
    else{
            return 'no';
        }  

        return 'error';


    }

    public function principal(){

        $lista_empresas = EMPRESA::get();

        $numero_empresas = $lista_empresas->count();

        $lista_usuarios = USUARIO::get();

        $numero_usuarios = $lista_usuarios->count();

        return view::make('index.principal')
        ->with('numero_empresas',$numero_empresas)
        ->with('numero_usuarios',$numero_usuarios);

    }

    public function salir(){

        session()->forget('perfil_uactivo');

    	return view::make('index.index');
    }

    public function forgot_password(){
        return view::make('index.forgot_password');
    }

    public function solicitar_correo_password(Request $request)
    {

        $correo = $request->input('correo');

        $sol = USUARIO::join('PERSONA as p','p.idUsuario','=','USUARIO.idUsuario')
        ->where('p.correo',$correo);

        
        if($sol->count() > 0){

            $data = [


                'nom_usuario' => $sol->first()->nombreUsuario,
                'pass_usuario' => $sol->first()->contra,
                'nombre_usuario' => $sol->first()->nombre,

            ];

            $mail = \Mail::to($correo)->send(new forgot_password($data));

            return $correo;

        }

        else{
            return 'Correo no registrado';
        }
               

    }

}
