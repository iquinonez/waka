<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App\USUARIO;
use App\PERSONA;
use App\PERFILXOPCION;
use App\USUARIOXPERFIL;
use App\gerencias;
use App\gerentes;
use App\jefaturas;
use App\jefes;
use App\ingenieros;
use App\comerciales;
use Illuminate\Support\Facades\DB;
use App\Classes\dsClass;

class usuariosController extends Controller
{

    public function index()
    {

        // $ee = [''];
        // $dsClass = new dsClass();
        // $tab_USUARIO = $dsClass->modelMaster('fn_AudiUsuarioLis',$ee);

        $tab_USUARIO =  USUARIO::join('PERSONA as p','p.idUsuario','=','USUARIO.idUsuario')
            ->leftjoin('USUARIOXPERFIL as up','up.idUsuario','=','USUARIO.idUsuario')
            ->leftjoin('PERFIL as per','per.idPerfil','=','up.idPerfil')
            ->select(
                'USUARIO.idUsuario',
                'USUARIO.nombreUsuario',
                'USUARIO.estado',
                'p.idPersona',
                'p.nombre',
                'p.telefono',
                'p.correo',
                'p.codigotipodocumento',
                'p.nrodocumento',
                'p.apellido',
                'up.idPerfil',
                'up.fechaAsig',
                'up.usuarioAsig',
                'per.nombre as nombreperfil'
            )
            ->get();
        
        $gerencias = gerencias::get();

        return view::make('usuarios.index')
            ->with('tab_USUARIO',$tab_USUARIO)
            ->with('gerencias',$gerencias);

    }

    public function bucar_jefaturas(Request $request)
    {
        $id_gerencia = $request->input('id_gerencia');

        $jefaturas = jefaturas::where('idgerencia',$id_gerencia)->get();

        return $jefaturas;
    }

    public function crearusuario(Request $request)
    {

        $nombreUsuario = $request->input( 'correoPersona' );
        $contraUsuario = $request->input( 'contraUsuario' );
        $nombrePersona = $request->input( 'nombrePersona' );
        $apellidoPersona = $request->input('apellidoPersona');
        $telefonoPersona = $request->input( 'telefonoPersona' );
        $correoPersona = $request->input( 'correoPersona' );
        $tipodocPersona = $request->input( 'tipodocPersona' );
        $docPersona = $request->input( 'docPersona' );

        $contador_personas = PERSONA::get();

        if($contador_personas->count() > 0){
            $sgt_id = PERSONA::orderBy('idPersona','DESC')->first()->idPersona+1;
        }
        else{
            $sgt_id = 1;
        }

        //$w = [$nombreUsuario,$contraUsuario,$nombrePersona,$cargoPersona,$telefonoPersona,$correoPersona,$tipodocPersona,$docPersona];
        //$dsClass = new dsClass();
        //$fn_au = $dsClass->modelMaster('fn_audiusuarionue',$w);
        //return $fn_au;

        $nuevo_usuario = new USUARIO;

        $nuevo_usuario->nombreUsuario = $nombreUsuario;
        $nuevo_usuario->contra = sha1($contraUsuario);
        $nuevo_usuario->estado = 1;

        $nuevo_usuario->save();

        $nuevo_persona = new PERSONA;

        $nuevo_persona->idPersona = $sgt_id;
        $nuevo_persona->idUsuario = $nuevo_usuario->idUsuario;
        $nuevo_persona->nombre = $nombrePersona;
        $nuevo_persona->apellido = $apellidoPersona;

        $nuevo_persona->telefono = $telefonoPersona;
        $nuevo_persona->correo = $correoPersona;
        $nuevo_persona->codigotipodocumento = $tipodocPersona;
        $nuevo_persona->nrodocumento = $docPersona;

        $nuevo_persona->save();

        return 'ok';



    }

    public function consultar_perfil(Request $request)
    {
        $id_usuario_actual = $request->input( 'id_usuario_actual' );

        $perfil_solicitado = USUARIOXPERFIL::where('idUsuario',$id_usuario_actual)
            
            ->first();

        $idpersona = PERSONA::where('idUsuario',$id_usuario_actual)
                        ->first()
                        ->idPersona;

        if($perfil_solicitado != '' || $perfil_solicitado != null)
        {

            if($perfil_solicitado->idPerfil == 1)//Administrador
            {

                    $perfil_solicitado['gerencia'] = 0;
                    $perfil_solicitado['jefatura'] = 0;

            }

            if($perfil_solicitado->idPerfil == 2)//Gerente
            {

                $gerencia = gerentes::where('idpersona',$idpersona)
                                ->where('estado',1)
                                ->first();

                if($gerencia == '' || $gerencia == null){

                    $perfil_solicitado['gerencia'] = 0;
                    $perfil_solicitado['jefatura'] = 0;

                }
                else{

                    $perfil_solicitado['gerencia'] = $gerencia->idgerencia;
                    $perfil_solicitado['nom_gerencia'] = gerencias::where('idgerencia',$gerencia->idgerencia)->where('estado',1)->first()->nombregerencia;
                    $perfil_solicitado['jefatura'] = 0;
                    $perfil_solicitado['nom_jefatura'] = '';

                }
                
            }
            if($perfil_solicitado->idPerfil == 3)//Jefe
            {
                $jefatura = jefes::where('idpersona',$idpersona)->where('estado',1)->first();
                if($jefatura == '' || $jefatura == null){

                    $perfil_solicitado['gerencia'] = 0;
                    $perfil_solicitado['jefatura'] = 0;
                    
                }
                else{

                    $perfil_solicitado['gerencia'] = $jefatura->idgerencia;
                    $perfil_solicitado['jefatura'] = $jefatura->idjefatura;
                    $perfil_solicitado['nom_gerencia'] = gerencias::where('idgerencia',$jefatura->idgerencia)->where('estado',1)->first()->nombregerencia;
                    $perfil_solicitado['nom_jefatura'] = jefaturas::where('idjefatura',$jefatura->idjefatura)->where('estado',1)->first()->nombrejefatura;

                }
                
            }
            if($perfil_solicitado->idPerfil == 4)//Comercial
            {
                $comerciales = comerciales::where('idpersona',$idpersona)
                                ->where('estado',1)
                                ->first();

                if($comerciales == '' || $comerciales == null){

                    $perfil_solicitado['gerencia'] = 0;
                    $perfil_solicitado['jefatura'] = 0;

                }
                else{

                    $perfil_solicitado['gerencia'] = $comerciales->idgerencia;
                    $perfil_solicitado['jefatura'] = $comerciales->idjefatura;
                    $perfil_solicitado['nom_gerencia'] = gerencias::where('idgerencia',$comerciales->idgerencia)->first()->nombregerencia;
                    $perfil_solicitado['nom_jefatura'] = jefaturas::where('idjefatura',$comerciales->idjefatura)->first()->nombrejefatura;

                }
                
            }
            if($perfil_solicitado->idPerfil == 5)//Ingeniero
            {
                $ingenieros = ingenieros::where('idpersona',$idpersona)
                                ->where('estado',1)
                                ->first();

                if($ingenieros == '' || $ingenieros == null){

                    $perfil_solicitado['gerencia'] = 0;
                    $perfil_solicitado['jefatura'] = 0;

                }
                else{

                    $perfil_solicitado['gerencia'] = $ingenieros->idgerencia;
                    $perfil_solicitado['jefatura'] = $ingenieros->idjefatura;
                    $perfil_solicitado['nom_gerencia'] = gerencias::where('idgerencia',$ingenieros->idgerencia)->first()->nombregerencia;
                    $perfil_solicitado['nom_jefatura'] = jefaturas::where('idjefatura',$ingenieros->idjefatura)->first()->nombrejefatura;

                }
                
            }


        }

        return $perfil_solicitado;

    }

    public function modificar_perfil(Request $request)
    {

        $id_usuario_actual = $request->input( 'id_usuario_actual' );
        $id_perfil = $request->input( 'id_perfil' );

        $asignarGerencia = $request->input( 'asignarGerencia' );
        $asignarJefatura = $request->input( 'asignarJefatura' );

        $id_persona = PERSONA::where('idUsuario',$id_usuario_actual)->first()->idPersona;


        if($asignarGerencia != '' || $asignarGerencia != null){

            $datos_gerente_actual = gerentes::where('idgerencia',$asignarGerencia)
                                    ->first();

        }
                            

        if($asignarJefatura != '' || $asignarJefatura != null){

            $datos_jefe_actual = jefes::where('idjefatura',$asignarJefatura)
                                    ->where('idgerencia',$asignarGerencia)
                                    ->first();

        }





    //SIN ASIGNAR
    if ($id_perfil == 0) {

        $this->check_perfil($id_persona);

    }


    //ADMIN
    if ($id_perfil == 1) {

        $this->check_perfil($id_persona);

    }

    //GERENTE
    if ($id_perfil == 2) {


        
        $this->check_perfil($id_persona,$asignarGerencia,$asignarJefatura);



            $nuevo  = new gerentes;
                $nuevo->idgerencia = $asignarGerencia;
                $nuevo->idpersona = $id_persona;
                $nuevo->estado = 1;
            $nuevo->save();

    }

    //JEFE
    if ($id_perfil == 3) {

        $this->check_perfil($id_persona,$asignarGerencia,$asignarJefatura);

            $nuevo  = new jefes;
                $nuevo->idjefatura = $asignarJefatura;
                $nuevo->idgerencia = $asignarGerencia;
                $nuevo->idpersona = $id_persona;
                $nuevo->estado = 1;
            $nuevo->save();

    }

    //COMERCIAL
    if ($id_perfil == 4 ) {    

        $this->check_perfil($id_persona);   
        
            $nuevo  = new comerciales;
                $nuevo->idjefatura = $asignarJefatura;
                $nuevo->idgerencia = $asignarGerencia;
                $nuevo->idpersona = $id_persona;
                $nuevo->estado = 1;
            $nuevo->save();

    }

    //INGENIERO
    if ($id_perfil == 5 ) {       

        $this->check_perfil($id_persona);

            $nuevo  = new ingenieros;
                $nuevo->idjefatura = $asignarJefatura;
                $nuevo->idgerencia = $asignarGerencia;
                $nuevo->idpersona = $id_persona;
                $nuevo->estado = 1;
            $nuevo->save();

    }
    
    $wa = USUARIOXPERFIL::where('idUsuario',$id_usuario_actual)->count();

    if($wa == 0){
        
        $ww = new USUARIOXPERFIL;
        $ww->idUsuario = $id_usuario_actual;
        $ww->idPerfil = $id_perfil;
        $ww->fechaAsig = carbon::now()->format('Y-m-d');
        $ww->usuarioAsig = 1;
        $ww->save();

    }


    $mod  = USUARIOXPERFIL::where('idUsuario',$id_usuario_actual)
                ->update(
                    ['idPerfil' => $id_perfil]
                );

    
        

        /*$w = [$id_usuario_actual,$id_perfil];
        $dsClass = new dsClass();
        $fn_au = $dsClass->modelMaster('fn_AudiUsuarioPerfilAsigCtl',$w);*/


        return "ok";

    }

    private function check_perfil($id_persona,$asignarGerencia = 0,$asignarJefatura = 0){


        if($asignarJefatura != 0 && $asignarGerencia != 0){//Para Jefes

            $jefe_actual = jefes::where('idjefatura',$asignarJefatura)
                    ->join('PERSONA as p','p.idPersona','=','jefes.idpersona')
                    ->where('idgerencia',$asignarGerencia)
                    ->where('estado',1)
                    ->first();

            jefes::where('idjefe',$jefe_actual->idjefe)
                    ->update(['estado'=>0]);

            USUARIOXPERFIL::where('idUsuario',$jefe_actual->idUsuario)
                    ->update(['idPerfil' => 0 ]);


            

        }


        if($asignarJefatura == 0 && $asignarGerencia != 0){//Para Gerentes

            $gerente_actual = gerentes::where('idgerencia',$asignarGerencia)
                    ->join('PERSONA as p','p.idPersona','=','gerentes.idpersona')
                    ->where('estado',1)
                    ->first();

            gerentes::where('idgerente',$gerente_actual->idgerente)
                    ->update(['estado'=>0]);

            USUARIOXPERFIL::where('idUsuario',$gerente_actual->idUsuario)
                    ->update(['idPerfil' => 0 ]);
            




        }

        $existe_gerente = gerentes::where('idpersona',$id_persona)
                            ->where('estado',1)
                            ->get();

        $existe_jefe = jefes::where('idpersona',$id_persona)
                            ->where('estado',1)
                            ->get();

        $existe_comercial = comerciales::where('idpersona',$id_persona)
                            ->where('estado',1)
                            ->get();

        $existe_ingeniero = ingenieros::where('idpersona',$id_persona)
                            ->where('estado',1)
                            ->get();

        if(count($existe_gerente) > 0){
            gerentes::where('idpersona',$id_persona)
                        ->update(['estado'=>0]);
        }

        if(count($existe_jefe)>0){
            jefes::where('idpersona',$id_persona)
                        ->update(['estado'=>0]);
        }

        if(count($existe_comercial) > 0){
            comerciales::where('idpersona',$id_persona)
                        ->update(['estado'=>0]);
        }

        if(count($existe_ingeniero)>0){
            ingenieros::where('idpersona',$id_persona)
                        ->update(['estado'=>0]);
        }



    }

    public function consultar_datos_usuario_persona(Request $request)
    {
        $id_usuario_actual = $request->input( 'id_usuario_actual' );

        $consulta_datos = USUARIO::leftJoin('PERSONA as p','p.idUsuario','=','USUARIO.idUsuario')
            ->where('USUARIO.idUsuario',$id_usuario_actual)
            ->get()
            ->first();

        return $consulta_datos;

    }

    public function editarusuariopersona(Request $request)
    {

        $nombrePersonaEditar = $request->input( 'nombrePersonaEditar' );
        $apellidoPersonaEditar = $request->input('apellidoPersonaEditar');

        $contraPersonaEditar = $request->input('passwordPersonaEditar');

        $telefonoPersonaEditar = $request->input( 'telefonoPersonaEditar' );
        $correoPersonaEditar = $request->input( 'correoPersonaEditar' );

        $docPersonaEditar = $request->input('docPersonaEditar');

        $id_usuario_actual = $request->input( 'id_usuario_actual' );

        $id_persona_actual = $request->input( 'id_persona_actual' );

        $id_perfil_actual = $request->input( 'id_perfil_actual' );


        

        $busc = PERSONA::where('idPersona',$id_persona_actual)->update([
            'nombre'=>$nombrePersonaEditar,
            'apellido'=>$apellidoPersonaEditar,
            'correo'=>$correoPersonaEditar,
            'telefono'=>$telefonoPersonaEditar,
            'nrodocumento'=>$docPersonaEditar
        ]);

        if($contraPersonaEditar == ''){

            $busc2 = USUARIO::where('idUsuario',$id_usuario_actual)
            ->update([
                'nombreUsuario'=>$correoPersonaEditar
            ]);

        }
        else{

            $busc2 = USUARIO::where('idUsuario',$id_usuario_actual)
            ->update([
                'nombreUsuario'=>$correoPersonaEditar,
                'contra'=>SHA1($contraPersonaEditar)
            ]);

        }

        
        

        if($busc){
            if($busc2)
            {
                return 'ok';
            }
            else{
                return 'no';
            }
            
        }
        else{
            return 'no';
        }

    }

    public function editarestado(Request $request)
    {

        $id_usuario_actual = $request->input( 'id_usuario_actual' );

        $estado = $request->input( 'estado' );

        $busc = USUARIO::where('idUsuario',$id_usuario_actual)->first()->idUsuario;
        
        $modificar = USUARIO::find($busc);

        $modificar->estado = $estado;

        $modificar->save();

        if($modificar){
            return 'ok';
        }
        else{
            return 'no';
        }

    }


    public function eliminarusuario($id)
    {

        $busc = USUARIOXPERFIL::where('id',$id)->first()->id;

        $request = USUARIOXPERFIL::find($busc);

        $request->delete();
        
        return 'ok';
    }
}
