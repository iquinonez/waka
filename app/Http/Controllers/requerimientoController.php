<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App\USUARIO;
use App\PERSONA;
use App\EMPRESA;
use App\PERFILXOPCION;
use App\USUARIOXPERFIL;
use Illuminate\Support\Facades\DB;
use App\Classes\dsClass;


use App\oportunidades;
use App\soldedicadas;
use App\sedes;
use App\archivo;
use App\adetalle;
use App\tdr;
use App\tdrcoes;

use App\jefes;
use App\gerentes;
use App\jefaturas;
use App\gerencias;

use Excel;

class requerimientoController extends Controller
{

    public function index()
    {


    	return view::make('requerimiento.index');

    }

     public function guardarrequerimiento(Request $request)
    {   
        //$nrosolicitud=$request->input('nrosolicitud');
        //$solicitante = $request->input('solicitante');
        $id= $request->input('id');//id del usuario
       // $correo = $request->input('correo');
        //$telefono = $request->input('telefono');
        $supervision = $request->input('supervision');
        $jefatura = $request->input('jefatura');
        $gerencia = $request->input('gerencia');
        $motivo = $request->input('motivo');
        $fecha= $request->input('fecha');
        $fecha= Carbon::parse($fecha)->format('Y-m-d');
        $agregar1 = new tdrcoes;
            $agregar1->idsolicitante=$id;
            //$agregar1->solicitante = $solicitante;
            $agregar1->supervision = $supervision;
            $agregar1->jefatura = $jefatura;
            $agregar1->fecha = $fecha;
            $agregar1->gerencia = $gerencia;
            $agregar1->motivo = $motivo;
            $agregar1->estado=1;
        $agregar1->save();

        return $agregar1;
    }

    public function info_usuario(Request $request)
     {  
        $ide=$request->input('id');
        $info = USUARIO::join('PERSONA as per','per.idUsuario','=','USUARIO.idUsuario')
            ->where('USUARIO.idUsuario',$ide)->get();

        return $info;

     }

     public function lista_tdr(Request $request)
     {  
        $oportunidades = tdrcoes::where('estadof','!=',1)
                            ->join('PERSONA as per','per.idUsuario','=','tdrcoes.idsolicitante');
        if (session('cargo')==5) {
            $oportunidades->where('idsolicitante',session('id_ing'))
            ->orwhere('id_ing',session('id_ing'));      
        }
        $op2= $oportunidades->select('tdrcoes.id as id',
                                    'tdrcoes.idsolicitante',
                                    'per.nombre as solicitante',
                                    'per.correo as correo',
                                    'per.telefono as telefono',
                                    'tdrcoes.supervision',
                                    'tdrcoes.jefatura',
                                    'tdrcoes.gerencia',
                                    'tdrcoes.fecha',
                                    'tdrcoes.estado',
                                    'tdrcoes.id_ing'
                                    )->get();

        return view::make('requerimiento.lista')
            ->with('oportunidades',$op2);

     }

     public function despachar_oportunidad(Request $request)
    {
        $id = $request->input('id_op_el');
        $comentario = $request->input('com');
        $estado = $request->input('est');

        tdrcoes::where('id',$id)
            ->update(['estado'=>$estado,'comentario_lista'=>$comentario,'fecha_comentario'=>Carbon::now()]);
        if($estado == 2)
        {
            tdrcoes::where('id',$id)
                ->update(['fecha_comentario'=>Carbon::now()]);

            tdrcoes::where('id',$id)
                ->update(['estado'=>2,'fecha_estado' => Carbon::now()]);

            return 'Oportunidad Aprobada';
        }
        if($estado == 3)
        {
            tdrcoes::where('id',$id)
                ->update(['estado'=>3,'estadof'=>1,'fecha_estado' => Carbon::now()]);  
            return 'Oportunidad Desaprobada';
        }
        if($estado == 6)
        {
            tdrcoes::where('id',$id)
                ->update(['estado'=>6,'fecha_estado' => Carbon::now()]);  
            return 'Oportunidad Aprobada';
        }
        if($estado == 7)
        {
            tdrcoes::where('id',$id)
                ->update(['estado'=>7,'estadof'=>1,'fecha_estado' => Carbon::now()]);  
            return 'Oportunidad Desaprobada';
        }
        if($estado == 8)
        {
            tdrcoes::where('id',$id)
                ->update(['estado'=>8,'fecha_estado' => Carbon::now()]);  
            return 'Requerimiento Aprobado';
        }
        if($estado == 9)
        {
            tdrcoes::where('id',$id)
                ->update(['estado'=>9,'estadof'=>1,'fecha_estado' => Carbon::now()]);  
            return 'Requerimiento Desaprobado';
        }
    }

    public function guardar_final(Request $request){
        $id=$request->input('idde');
        $fecha_ejecucion= $request->input('fecha_e');
        $fecha_ejecucion=Carbon::parse($fecha_ejecucion)->format('Y-m-d');
        $fecha_recepcion= $request->input('fecha_r');
        $fecha_recepcion=Carbon::parse($fecha_recepcion)->format('Y-m-d');
        $atendido= $request->input('atend');
        $respuestaredes= $request->input('rredes');
        $usuario= $request->input('usu');
        $password= $request->input('pass');

        tdrcoes::where('id',$id)
            ->update(['fecha_e'=>$fecha_ejecucion,'estado'=>5,'fecha_r'=>$fecha_recepcion,'atendido'=>$atendido,'respuesta'=>$respuestaredes,'usuario'=>$usuario,'password'=>$password]);

        return "Listo";

    }

    public function lista_tdr2(Request $request)
    {  
        $estadof= $request->input('choose');

        $oportunidades = tdrcoes::where('estadof','=',$estadof)
                            ->join('PERSONA as per','per.idUsuario','=','tdrcoes.idsolicitante')
                            ->select('tdrcoes.id as id',
                                    'tdrcoes.idsolicitante',
                                    'per.nombre as solicitante',
                                    'per.correo as correo',
                                    'per.telefono as telefono',
                                    'tdrcoes.supervision',
                                    'tdrcoes.jefatura',
                                    'tdrcoes.gerencia',
                                    'tdrcoes.fecha',
                                    'tdrcoes.estado',
                                    'tdrcoes.estadof'
                                    )->get();
        return $oportunidades;
    }


    public function versolucion(Request $request){

       $idde= $request->input('idde');
       $respuesta =  tdrcoes::where('id',$idde)
                            ->join('PERSONA as per','per.idUsuario','=','tdrcoes.idsolicitante')
                            ->select('tdrcoes.id as id',
                                    'tdrcoes.idsolicitante',
                                    'per.nombre as solicitante',
                                    'per.correo as correo',
                                    'per.telefono as telefono',
                                    'tdrcoes.supervision',
                                    'tdrcoes.jefatura',
                                    'tdrcoes.gerencia',
                                    'tdrcoes.fecha',
                                    'tdrcoes.estado',
                                    'tdrcoes.motivo'
                                )->get();

        return $respuesta[0];
    }

    public function versolucion2(Request $request){

       $idde= $request->input('idde');
       $respuesta =  tdrcoes::where('id',$idde)
                            ->join('PERSONA as per','per.idUsuario','=','tdrcoes.idsolicitante')
                            ->select('tdrcoes.id as id',
                                    'tdrcoes.idsolicitante',
                                    'per.nombre as solicitante',
                                    'per.correo as correo',
                                    'per.telefono as telefono',
                                    'tdrcoes.supervision',
                                    'tdrcoes.jefatura',
                                    'tdrcoes.gerencia',
                                    'tdrcoes.fecha',
                                    'tdrcoes.estado',
                                    'tdrcoes.motivo',
                                    'tdrcoes.fecha_e',
                                    'tdrcoes.fecha_r',
                                    'tdrcoes.atendido',
                                    'tdrcoes.respuesta',
                                    'tdrcoes.usuario',
                                    'tdrcoes.password'
                                )->get();

        return $respuesta[0];
    }
}
