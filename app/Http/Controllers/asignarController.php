<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use View;
use Response;
use App\USUARIO;
use App\PERSONA;
use App\EMPRESA;
use App\PERFILXOPCION;
use App\USUARIOXPERFIL;
use App\ingenieros;
use App\oportunidades;
use Illuminate\Support\Facades\DB;
use App\Classes\dsClass;
use App\Mail\forgot_password;
use Hash;
use App\tdrcoes;


class asignarController extends Controller
{

    public function index()
    {

        $oportunidades = tdrcoes::orderBy('id','DESC')->where('estado',2)
                            ->get();



            
        $ingenieros = PERSONA::join('ingenieros as i','i.idpersona','PERSONA.idPersona')
            ->where('i.estado',1)
            ->where('i.idgerencia',1)
            ->where('i.idjefatura',1)
            ->orderBy('idPersona','DESC')
            ->select(
                'i.idingeniero',
                'PERSONA.idPersona',
                'PERSONA.nombre',
                'PERSONA.apellido'
            )
            ->distinct()
            ->get();

        //return session('perfil_uactivo');
    	return view::make('asignar.index')
            ->with('ingenieros',$ingenieros)
            ->with('o2',$oportunidades);
    }

    

    public function asignar_ingeniero(Request $request){

        $id=$request->input('ide');
        $idingeniero=$request->input('idingeniero');
        $idpersona=$request->input('idpersona');
        $comentario=$request->input('comentario');
        $q= ingenieros::join('PERSONA as p','p.idPersona','=','ingenieros.idpersona')
                        ->where('ingenieros.idingeniero',$idingeniero)->first();

        tdrcoes::where('id',$id)
                ->update(['id_ing'=>$q['idUsuario'],'id_persona' => $idpersona,'comentario_asignado'=>$comentario,'estado'=>4]);
       return "Asignación Exitosa";
    }

}
