<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class soldedicadas extends Model
{
	protected $primaryKey = 'idsd';
    protected $table = 'soldedicadas';
    public $timestamps = false;
}
