<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comerciales extends Model
{
	protected $primaryKey = 'idcomercial';
    protected $table = 'comerciales';
    public $timestamps = false;
}
