<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PERFILXOPCION extends Model
{
	protected $primaryKey = 'idPerfil';
    protected $table = 'PERFILXOPCION';
    public $timestamps = false;
}
