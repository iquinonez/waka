<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aceptacion extends Model
{
	protected $primaryKey = 'idaceptacion';
    protected $table = 'aceptacion';
    public $timestamps = false;
}
