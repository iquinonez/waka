<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jefes extends Model
{
	protected $primaryKey = 'idjefe';
    protected $table = 'jefes';
    public $timestamps = false;
}
