<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class USUARIOXPERFIL extends Model
{
	protected $primaryKey = "idUsuario";
    protected $table = 'USUARIOXPERFIL';
    public $timestamps = false;
    protected $fillable = array( 'idPerfil');
}
