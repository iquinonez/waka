<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gerencias extends Model
{
	protected $primaryKey = 'idgerencia';
    protected $table = 'gerencias';
    public $timestamps = false;
}
