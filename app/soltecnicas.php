<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class soltecnicas extends Model
{
	protected $primaryKey = 'idst';
    protected $table = 'soltecnicas';
    public $timestamps = false;
}
