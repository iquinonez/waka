<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tdrcoes extends Model
{
	protected $primaryKey = 'id';
    protected $table = 'tdrcoes';
    public $timestamps = false;
}
